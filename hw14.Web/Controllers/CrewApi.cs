/*
 * Imaginary airport
 *
 * Airport dispatcher API
 *
 * OpenAPI spec version: 0.0.42
 *
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.SwaggerGen;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using hw14.Web.Attributes;
using hw14.Dto;
using hw14.Models;
using hw14.Services;
using hw14.DAL;

namespace hw14.Web.Controllers
{
    /// <summary>
    ///
    /// </summary>
    public class CrewApiController : Controller
    {
        private ICrewService service;
        public CrewApiController(ICrewService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Получить список экипажей
        /// </summary>

        /// <response code="200">OK</response>
        [HttpGet]
        [Route("/api/v1/crews")]
        [ValidateModelState]
        [SwaggerOperation("CrewsGet")]
        [SwaggerResponse(statusCode: 200, type: typeof(IEnumerable<CrewDto>), description: "OK")]
        public virtual Task<IEnumerable<CrewDto>> CrewsGet() =>
            service.GetList();

        /// <summary>
        /// Получить состав экипажа
        /// </summary>

        /// <param name="id"></param>
        /// <response code="200">OK</response>
        /// <response code="400">Ошибка валидации входных данных</response>
        /// <response code="404">Ресурс не найден</response>
        [HttpGet]
        [Route("/api/v1/crews/{id}")]
        [ValidateModelState]
        [SwaggerOperation("CrewsIdGet")]
        [SwaggerResponse(statusCode: 200, type: typeof(CrewDto), description: "OK")]
        public virtual Task<CrewDto> CrewsIdGet([FromRoute][Required]long id) =>
            service.GetById(id);

        /// <summary>
        /// Создать новый экипаж
        /// </summary>

        /// <param name="body"></param>
        /// <response code="200">Ресурс создан</response>
        /// <response code="400">Ошибка валидации входных данных</response>
        [HttpPost]
        [Route("/api/v1/crews")]
        [ValidateModelState]
        [SwaggerOperation("CrewsPost")]
        [SwaggerResponse(statusCode: 200, type: typeof(CrewDto), description: "OK")]
        public virtual Task<CrewDto> CrewsPost([FromBody]CrewDto body) =>
            service.CreateNew(body);

        /// <summary>
        /// Получить список стюардесс экипажа
        /// </summary>

        /// <param name="crewId"></param>
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("/api/v1/crews/{crewId}/stewardess")]
        [ValidateModelState]
        [SwaggerOperation("CrewsCrewIdStewardessGet")]
        [SwaggerResponse(statusCode: 200, type: typeof(IEnumerable<StewardessDto>), description: "OK")]
        public virtual Task<IEnumerable<StewardessDto>> CrewsCrewIdStewardessGet([FromRoute][Required]long crewId) =>
            service.GetStewardessList(crewId);

        /// <summary>
        /// Исключить стюардессу из экипажа
        /// </summary>

        /// <param name="crewId"></param>
        /// <param name="id"></param>
        /// <response code="204">Ресурс удален</response>
        /// <response code="400">Ошибка валидации входных данных</response>
        /// <response code="404">Ресурс не найден</response>
        [HttpDelete]
        [Route("/api/v1/crews/{crewId}/stewardess/{id}")]
        [ValidateModelState]
        [SwaggerOperation("CrewsCrewIdStewardessIdDelete")]
        [SwaggerResponse(statusCode: 200, type: typeof(StewardessDto), description: "OK")]
        public virtual async Task<IActionResult> CrewsCrewIdStewardessIdDelete([FromRoute][Required]long crewId, [FromRoute][Required]long id)
        {
            await service.RemoveStewardess(crewId, id);

            return StatusCode(204);
        }

        /// <summary>
        /// Добавить стюардессу в экипаж
        /// </summary>

        /// <param name="crewId"></param>
        /// <param name="id"></param>
        /// <response code="200">Ресурс создан</response>
        /// <response code="400">Ошибка валидации входных данных</response>
        /// <response code="404">Ресурс не найден</response>
        /// <response code="409">Этот ресурс используется в других объектах</response>
        [HttpPost]
        [Route("/api/v1/crews/{crewId}/stewardess/{id}")]
        [ValidateModelState]
        [SwaggerOperation("CrewsCrewIdStewardessIdPost")]
        [SwaggerResponse(statusCode: 200, type: typeof(StewardessDto), description: "OK")]
        public virtual Task<StewardessDto> CrewsCrewIdStewardessIdPost([FromRoute][Required]long crewId, [FromRoute][Required]long id) =>
            service.AddStewardess(crewId, id);
    }
}
