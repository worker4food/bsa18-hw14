/*
 * Imaginary airport
 *
 * Airport dispatcher API
 *
 * OpenAPI spec version: 0.0.42
 *
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace hw14.Models
{
    /// <summary>
    /// Crew model
    /// </summary>
    public partial class Crew : Entity<long>
    {
        /// <summary>
        /// Gets or Sets PilotId (foreign key)
        /// </summary>
        public long? PilotId { get; set; }

        /// <summary>
        /// Gets or Sets Pilot
        /// </summary>
        public Pilot Pilot { get; set; }

        /// <summary>
        /// Gets or Sets Stewardesses
        /// </summary>
        public IEnumerable<CrewStewardess> CrewStewardesses { get; set; }
    }
}
