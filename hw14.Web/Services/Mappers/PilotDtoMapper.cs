using AutoMapper;
using hw14.Dto;
using hw14.Models;

namespace hw14.Services.Mappers
{
    public class PilotDtoMapper : DefaultMapper<Pilot, PilotDto>
    {
        protected override IMapper from { get =>
            new MapperConfiguration(cfg => cfg.CreateMap<PilotDto, Pilot>()
                .ForMember(dest => dest.Crew,
                    opt => opt.Ignore())
            ).CreateMapper();
        }
    }
}
