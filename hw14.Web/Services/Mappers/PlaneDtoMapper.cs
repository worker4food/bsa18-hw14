using AutoMapper;
using hw14.Dto;
using hw14.Models;

namespace hw14.Services.Mappers
{
    public class PlaneDtoMapper : DefaultMapper<Plane, PlaneDto>
    {
        protected override IMapper from { get =>
            new MapperConfiguration(cfg =>
                cfg.CreateMap<PlaneDto, Plane>()
                    .ForMember(
                        p => p.PlaneType,
                        opt => opt.Ignore())
            ).CreateMapper();
        }
    }
}
