
namespace hw14.Services.Mappers
{
    public interface IDtoMapper<T, TDto>
    {
        TDto ToDto(T model);
        T FromDto(TDto dto);
        T FromDto(TDto dto, T model);

    }
}
