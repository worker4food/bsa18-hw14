using AutoMapper;
using hw14.Dto;
using hw14.Models;

namespace hw14.Services.Mappers
{
    public class StewardessDtoMapper : DefaultMapper<Stewardess, StewardessDto>
    {
        protected override IMapper from { get => new MapperConfiguration(cfg =>
           cfg.CreateMap<StewardessDto, Stewardess>()
                .ForMember(dest => dest.CrewStewardesses,
                    o => o.Ignore())
            ).CreateMapper();
        }
    }
}
