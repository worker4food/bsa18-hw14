using AutoMapper;
using hw14.Dto;
using hw14.Models;

namespace hw14.Services.Mappers
{
    public class FlightDtoMapper : DefaultMapper<Flight, FlightDto>
    {
        protected override IMapper from { get => new MapperConfiguration(cfg =>
           cfg.CreateMap<FlightDto, Flight>()
                .ForMember(dest => dest.Tickets,
                    o => o.Ignore())
            ).CreateMapper();
        }
    }
}
