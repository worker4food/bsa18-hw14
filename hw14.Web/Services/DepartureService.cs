using System;
using System.Threading.Tasks;
using hw14.Dto;
using hw14.Models;
using hw14.DAL;
using hw14.Services.Mappers;

namespace hw14.Services
{
    public class DepartureService : AsyncDummyAirService<long, Departure, DepartureDto>, IDepartureService
    {
        public DepartureService(
            IAsyncRepository<long, Departure> repo,
            IDtoMapper<Departure, DepartureDto> m) : base(repo, m)
        {
        }

        public async Task<DepartureDto> MakeDeparture(long id)
        {
            var d = await repo.Get(id);

            if(d == null)
                throw NotFoundEx(id);

            d.DepartureDate = DateTime.Now;
            await repo.Update(d);

            return mapper.ToDto(d);
        }

        public override Task<DepartureDto> CreateNew(DepartureDto item)
        {
            item.DepartureDate = default(DateTimeOffset);
            return base.CreateNew(item);
        }

        public override Task<DepartureDto> UpdateById(long id, DepartureDto item)
        {
            item.DepartureDate = default(DateTimeOffset);
            return base.UpdateById(item.Id ?? 0, item);
        }
    }
}
