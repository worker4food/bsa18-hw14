using hw14.Dto;
using hw14.Models;

namespace hw14.Services
{
    public interface IPlaneTypeService: IAsyncAirService<long, PlaneType, PlaneTypeDto> { }
}
