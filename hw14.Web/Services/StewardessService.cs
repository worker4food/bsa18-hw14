using hw14.Dto;
using hw14.Models;
using hw14.DAL;
using hw14.Services.Mappers;

namespace hw14.Services
{
    public class StewardessService : AsyncDummyAirService<long, Stewardess, StewardessDto>, IStewardessService
    {
        public StewardessService(IAsyncRepository<long, Stewardess> repo, IDtoMapper<Stewardess, StewardessDto> m) : base(repo, m)
        {
        }
    }
}
