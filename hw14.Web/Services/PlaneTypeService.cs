using System;
using System.Linq;
using hw14.Dto;
using hw14.Models;
using hw14.DAL;
using hw14.Services.Mappers;

namespace hw14.Services
{
    public class PlaneTypeService: AsyncDummyAirService<long, PlaneType, PlaneTypeDto>, IPlaneTypeService
    {
        public PlaneTypeService(IAsyncRepository<long, PlaneType> r, IDtoMapper<PlaneType, PlaneTypeDto> m) : base(r, m)
        {}
    }
}
