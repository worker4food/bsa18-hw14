using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using hw14.DAL;
using hw14.Dto;
using hw14.Models;
using hw14.Services.Mappers;

namespace hw14.Services
{
    public class CrewService : AsyncDummyAirService<long, Crew, CrewDto>, ICrewService
    {
        protected IAsyncRepository<long, CrewStewardess> crewSrtewardessRepo;
        protected IDtoMapper<Stewardess, StewardessDto> stewMapper;
        public CrewService(
            IAsyncRepository<long, Crew> repo,
            IAsyncRepository<long, CrewStewardess> crewSrtewardessRepo,
            IDtoMapper<Crew, CrewDto> mapper,
            IDtoMapper<Stewardess, StewardessDto> stewMapper) : base(repo, mapper)
        {
            this.stewMapper = stewMapper;
            this.crewSrtewardessRepo = crewSrtewardessRepo;
        }

        public override async Task<IEnumerable<CrewDto>> GetList()
        {
            return await repo.GetAll()
                    .Include(x => x.CrewStewardesses)
                    .ThenInclude(x => x.Stewardess)
                    .Select(p => mapper.ToDto(p))
                    .ToListAsync();
        }

        public override async Task<CrewDto> CreateNew(CrewDto item)
        {
            item.Id = null;

            var newCrew = mapper.FromDto(item);
            var stewardesses = newCrew.CrewStewardesses;
            newCrew.CrewStewardesses = null;

            await repo.Insert(newCrew);

            newCrew.CrewStewardesses = stewardesses.Select(s => {
                s.CrewId = newCrew.Id;
                return s;
            }).ToList();

            await repo.Update(newCrew);

            return mapper.ToDto(newCrew);
        }

        public override async Task<CrewDto> GetById(long id)
        {
            var c = await repo.GetAll()
                .Include(x => x.CrewStewardesses)
                .ThenInclude(x => x.Stewardess)
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();

            if(c == null)
                throw NotFoundEx(id);

            return mapper.ToDto(c);
        }

        public async Task<IEnumerable<StewardessDto>> GetStewardessList(long id) =>
            (await GetById(id)).Stewardesses;

        public async Task<StewardessDto> AddStewardess(long crewId, long id)
        {
            var newCrewStew = new CrewStewardess {
                CrewId = crewId,
                StewardessId = id
            };

            await crewSrtewardessRepo.Insert(newCrewStew);

            return stewMapper.ToDto(newCrewStew.Stewardess);
        }

        public async Task<StewardessDto> RemoveStewardess(long crewId, long id)
        {
            var toRemove = crewSrtewardessRepo.GetAll()
                .Single(x => x.CrewId == crewId && x.StewardessId == id);

            await crewSrtewardessRepo.Delete(toRemove.Id);

            return stewMapper.ToDto(toRemove.Stewardess);
        }
    }
}
