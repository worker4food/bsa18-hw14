using System.Threading.Tasks;
using hw14.Dto;
using hw14.Models;

namespace hw14.Services
{
    public interface IDepartureService : IAsyncAirService<long, Departure, DepartureDto>
    {
        Task<DepartureDto> MakeDeparture(long id);
    }
}
