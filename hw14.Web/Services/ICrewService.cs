using System.Collections.Generic;
using System.Threading.Tasks;
using hw14.Dto;
using hw14.Models;

namespace hw14.Services
{
    public interface ICrewService : IAsyncAirService<long, Crew, CrewDto>
    {
        Task<IEnumerable<StewardessDto>> GetStewardessList(long id);
        Task<StewardessDto> AddStewardess(long crewId, long id);
        Task<StewardessDto> RemoveStewardess(long crewId, long id);
    }
}
