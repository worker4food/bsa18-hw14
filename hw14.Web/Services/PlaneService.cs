using System;
using System.Linq;
using hw14.Dto;
using hw14.Models;
using hw14.DAL;
using hw14.Services.Mappers;

namespace hw14.Services
{
    public class PlaneService: AsyncDummyAirService<long, Plane, PlaneDto>, IPlaneService
    {
        public PlaneService(IAsyncRepository<long, Plane> r, IDtoMapper<Plane, PlaneDto> m) : base(r, m)
        {}
    }
}
