using hw14.Dto;
using hw14.Models;
using hw14.DAL;
using hw14.Services.Mappers;

namespace hw14.Services
{
    public class PilotService : AsyncDummyAirService<long, Pilot, PilotDto>, IPilotService
    {
        public PilotService(IAsyncRepository<long, Pilot> repo, IDtoMapper<Pilot, PilotDto> m) : base(repo, m)
        {
        }
    }
}
