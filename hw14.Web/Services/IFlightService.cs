using System.Threading.Tasks;
using hw14.Dto;
using hw14.Models;

namespace hw14.Services
{
    public interface IFlightService: IAsyncAirService<string, Flight, FlightDto> {
        Task<FlightDto> CreateNew(FlightSchedDto p);
        Task<TicketDto> BuyTicket(string flightId, TicketPrefDto opt);
        Task ReturnTicket(string flightId, TicketDto ticket);

    }
}
