using hw14.Dto;
using hw14.Models;

namespace hw14.Services
{
    public interface IStewardessService : IAsyncAirService<long, Stewardess, StewardessDto> {}
}
