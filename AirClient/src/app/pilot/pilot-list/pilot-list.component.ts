import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { PilotListDataSource } from './pilot-list-datasource';
import { PilotApiService, PilotDto } from '../../rest-connector';

@Component({
  selector: 'app-pilot-list',
  templateUrl: './pilot-list.component.html',
  styleUrls: ['./pilot-list.component.scss']
})
export class PilotListComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: PilotListDataSource;

  constructor(public pilotApi: PilotApiService) {}
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'firstName', 'lastName', 'exp', 'birthDate', 'commands'];

  ngOnInit() {
    this.dataSource = new PilotListDataSource(this.pilotApi, this.paginator, this.sort);
  }

}
