import { Component, OnInit } from '@angular/core';
import { PilotDto, PilotApiService } from '../../rest-connector';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, FormArray, FormControl, FormGroup } from 'ngx-strongly-typed-forms';


@Component({
  selector: 'app-pilot-details',
  templateUrl: './pilot-details.component.html',
  styleUrls: ['./pilot-details.component.scss']
})
export class PilotDetailsComponent implements OnInit {
  form = this.fb.group<PilotDto>({
    id: null,
    firstName: [null, Validators.required],
    lastName: [null, Validators.required],
    birthDate: [null, Validators.required],
    exp: null
  });

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private informer: MatSnackBar,
    private pilotApi: PilotApiService) { }

  ngOnInit() {
    const id = this.route.snapshot.params.id;

    if (id !== 'new') {
      this.pilotApi.pilotsIdGet(id)
        .subscribe(planeType => this.form.setValue(planeType));
    }
  }

  isNew(): boolean {
    return this.form.value.id === null;
  }

  showMsg(msg: string, isError = false) {
    this.informer.open(msg, 'Close', {
      verticalPosition: 'top',
      duration: 3000,
      panelClass: isError ? 'error-msg' : 'info-msg'
    });
  }

  title(): string {
    if (this.isNew()) {
      return 'New pilot';
    } else {
      return `Pilot #${this.form.value.id}`;
    }
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    const api = this.pilotApi;
    const val = this.form.value;
    const req = this.isNew() ? api.pilotsPost(val) : api.pilotsIdPut(val.id, val);

    req.subscribe(
      _ => this.showMsg('Saved'),
      _ => this.showMsg('Communication error. Data not saved', true));
  }
}
