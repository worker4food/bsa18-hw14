import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { PlaneTypeListDataSource } from './plane-type-list-datasource';
import { PlaneTypeApiService } from '../../rest-connector';

@Component({
  selector: 'app-plane-type-list',
  templateUrl: './plane-type-list.component.html',
  styleUrls: ['./plane-type-list.component.scss']
})
export class PlaneTypeListComponent implements OnInit {
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    dataSource: PlaneTypeListDataSource;

    constructor(public pilotApi: PlaneTypeApiService) {}
    /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
    displayedColumns = ['id', 'model', 'capacity', 'seats', 'commands'];

    ngOnInit() {
      this.dataSource = new PlaneTypeListDataSource(this.pilotApi, this.paginator, this.sort);
    }

  }
