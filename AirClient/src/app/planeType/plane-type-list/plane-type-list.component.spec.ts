
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaneTypeListComponent } from './plane-type-list.component';

describe('PlaneTypeListComponent', () => {
  let component: PlaneTypeListComponent;
  let fixture: ComponentFixture<PlaneTypeListComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaneTypeListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlaneTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
