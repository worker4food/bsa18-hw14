import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';
import { PlaneTypeDto, PlaneTypeApiService } from '../../rest-connector';

/**
 * Data source for the PlaneTypeList view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class PlaneTypeListDataSource extends DataSource<PlaneTypeDto> {
  data: PlaneTypeDto[] = [];

  constructor(public pilotApi: PlaneTypeApiService, private paginator: MatPaginator, private sort: MatSort) {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<PlaneTypeDto[]> {
    const dataO = this.pilotApi.planeTypesGet();

    dataO.subscribe(data => {
        this.data = data;
        this.paginator.length = data.length;
    });

    return merge(dataO, this.paginator.page, this.sort.sortChange).pipe(
      map(() => this.getPagedData(this.getSortedData(this.data))));

}

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: PlaneTypeDto[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.slice(startIndex, startIndex + this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: PlaneTypeDto[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    const isAsc = this.sort.direction === 'asc';
    const field = this.sort.active;
    return data.sort((a, b) =>
      (a[field] < b[field] ? -1 : 1) * (isAsc ? 1 : -1));
  }

}
