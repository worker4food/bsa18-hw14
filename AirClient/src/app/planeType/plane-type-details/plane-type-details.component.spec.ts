
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaneTypeDetailsComponent } from './plane-type-details.component';

describe('PlaneTypeDetailsComponent', () => {
  let component: PlaneTypeDetailsComponent;
  let fixture: ComponentFixture<PlaneTypeDetailsComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaneTypeDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlaneTypeDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
