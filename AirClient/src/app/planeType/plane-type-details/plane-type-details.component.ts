import { Component, ViewChild, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormBuilder, FormArray, FormControl, FormGroup } from 'ngx-strongly-typed-forms';
import { PlaneTypeDto, PlaneApiService, PlaneTypeApiService } from '../../rest-connector';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-plane-type-details',

  templateUrl: './plane-type-details.component.html',
  styleUrls: ['./plane-type-details.component.scss'],
})
export class PlaneTypeDetailsComponent implements OnInit {
  form = this.fb.group<PlaneTypeDto>({
    id: null,
    model: [null, Validators.required],
    seats: [null, [Validators.min(1), Validators.required]],
    capacity: [null, [Validators.min(1), Validators.required]]
  });

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private informer: MatSnackBar,
    private planeTypeApi: PlaneTypeApiService) { }

  ngOnInit() {
    const id = this.route.snapshot.params.id;

    if (id !== 'new') {
      this.planeTypeApi.planeTypesIdGet(id)
        .subscribe(planeType => this.form.setValue(planeType));
    }
  }

  isNew(): boolean {
    return this.form.value.id === null;
  }

  showMsg(msg: string, isError = false) {
    this.informer.open(msg, 'Close', {
      verticalPosition: 'top',
      duration: 3000,
      panelClass: isError ? 'error-msg' : 'info-msg'
    });
  }

  title(): string {
    if (this.isNew()) {
      return 'New plane type';
    } else {
      return `Plane type #${this.form.value.id}`;
    }
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    const api = this.planeTypeApi;
    const val = this.form.value;
    const req = this.isNew() ? api.planeTypesPost(val) : api.planeTypesIdPut(val.id, val);

    req.subscribe(
      _ => this.showMsg('Saved'),
      _ => this.showMsg('Communication error. Data not saved', true));
  }
}
