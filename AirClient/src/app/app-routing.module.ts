import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PilotListComponent } from './pilot/pilot-list/pilot-list.component';
import { PlaneTypeListComponent } from './planeType/plane-type-list/plane-type-list.component';
import { PlaneListComponent } from './plane/plane-list/plane-list.component';
import { StewardessListComponent } from './stewardess/stewardess-list/stewardess-list.component';
import { FlightListComponent } from './flight/flight-list/flight-list.component';
import { DepartureListComponent } from './departure/departure-list/departure-list.component';
import { CrewListComponent } from './crew/crew-list/crew-list.component';
import { PlaneTypeDetailsComponent } from './planeType/plane-type-details/plane-type-details.component';
import { PlaneDetailsComponent } from './plane/plane-details/plane-details.component';
import { PilotDetailsComponent } from './pilot/pilot-details/pilot-details.component';
import { StewardessDetailsComponent } from './stewardess/stewardess-details/stewardess-details.component';
import { CrewDetailsComponent } from './crew/crew-details/crew-details.component';
import { FlightDetailsComponent } from './flight/flight-details/flight-details.component';
import { DepartureDetailsComponent } from './departure/departure-details/departure-details.component';

const routes: Routes = [
  { path: 'planeTypes', component: PlaneTypeListComponent },
  { path: 'planeTypes/:id', component: PlaneTypeDetailsComponent },

  { path: 'planes', component: PlaneListComponent },
  { path: 'planes/:id', component: PlaneDetailsComponent },

  { path: 'pilots', component: PilotListComponent },
  { path: 'pilots/:id', component: PilotDetailsComponent },

  { path: 'stewardesses', component: StewardessListComponent },
  { path: 'stewardesses/:id', component: StewardessDetailsComponent },

  { path: 'crews', component: CrewListComponent },
  { path: 'crews/:id', component: CrewDetailsComponent },

  { path: 'flights', component: FlightListComponent },
  { path: 'flights/:id', component: FlightDetailsComponent },

  { path: 'departures', component: DepartureListComponent },
  { path: 'departures/:id', component: DepartureDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
