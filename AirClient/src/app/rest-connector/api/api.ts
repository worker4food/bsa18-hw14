


export * from './crewApi.service';
import { CrewApiService } from './crewApi.service';




export * from './departuresApi.service';
import { DeparturesApiService } from './departuresApi.service';




export * from './fetcherApi.service';
import { FetcherApiService } from './fetcherApi.service';




export * from './flightsApi.service';
import { FlightsApiService } from './flightsApi.service';




export * from './pilotApi.service';
import { PilotApiService } from './pilotApi.service';




export * from './planeApi.service';
import { PlaneApiService } from './planeApi.service';




export * from './planeTypeApi.service';
import { PlaneTypeApiService } from './planeTypeApi.service';




export * from './stewardessApi.service';
import { StewardessApiService } from './stewardessApi.service';



export const APIS = [CrewApiService, DeparturesApiService, FetcherApiService, FlightsApiService, PilotApiService, PlaneApiService, PlaneTypeApiService, StewardessApiService];

