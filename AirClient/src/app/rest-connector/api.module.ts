import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { Configuration } from './configuration';
import { HttpClient } from '@angular/common/http';




import { CrewApiService } from './api/crewApi.service';

import { DeparturesApiService } from './api/departuresApi.service';

import { FetcherApiService } from './api/fetcherApi.service';

import { FlightsApiService } from './api/flightsApi.service';

import { PilotApiService } from './api/pilotApi.service';

import { PlaneApiService } from './api/planeApi.service';

import { PlaneTypeApiService } from './api/planeTypeApi.service';

import { StewardessApiService } from './api/stewardessApi.service';



@NgModule({
  imports:      [],
  declarations: [],
  exports:      [],
  providers: [
    CrewApiService,
    DeparturesApiService,
    FetcherApiService,
    FlightsApiService,
    PilotApiService,
    PlaneApiService,
    PlaneTypeApiService,
    StewardessApiService ]
})
export class ApiModule {
    public static forRoot(configurationFactory: () => Configuration): ModuleWithProviders {
        return {
            ngModule: ApiModule,
            providers: [ { provide: Configuration, useFactory: configurationFactory } ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: ApiModule,
                 @Optional() http: HttpClient) {
        if (parentModule) {
            throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
            'See also https://github.com/angular/angular/issues/20575');
        }
    }
}
