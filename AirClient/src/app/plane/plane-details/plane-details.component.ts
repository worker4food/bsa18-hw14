import { Component, OnInit } from '@angular/core';
import { PlaneDto, PlaneApiService, PlaneTypeApiService, PlaneTypeDto } from '../../rest-connector';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, FormArray, FormControl, FormGroup } from 'ngx-strongly-typed-forms';

@Component({
  selector: 'app-plane-details',
  templateUrl: './plane-details.component.html',
  styleUrls: ['./plane-details.component.scss']
})
export class PlaneDetailsComponent implements OnInit {
  form = this.fb.group<PlaneDto>({
    id: null,
    name: [null, Validators.required],
    planeTypeId: [null, Validators.required],
    createdDate: [null, Validators.required],
    lifetime: [null, [Validators.min(1), Validators.required]]
  });

  planeTypes: PlaneTypeDto[] = [];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private informer: MatSnackBar,
    private planeApi: PlaneApiService,
    private planeTypeApi: PlaneTypeApiService) { }

  ngOnInit() {
    const id = this.route.snapshot.params.id;

    if (id !== 'new') {
      this.planeApi.planesIdGet(id)
        .subscribe(planeType => this.form.setValue(planeType));
    }

    this.planeTypeApi.planeTypesGet()
      .subscribe(pt => { this.planeTypes = pt; });
  }

  isNew(): boolean {
    return this.form.value.id === null;
  }

  showMsg(msg: string, isError = false) {
    this.informer.open(msg, 'Close', {
      verticalPosition: 'top',
      duration: 3000,
      panelClass: isError ? 'error-msg' : 'info-msg'
    });
  }

  title(): string {
    if (this.isNew()) {
      return 'New plane';
    } else {
      return `Plane #${this.form.value.id}`;
    }
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    const api = this.planeApi;
    const val = this.form.value;
    const req = this.isNew() ? api.planesPost(val) : api.planesIdPut(val.id, val);

    req.subscribe(
      _ => this.showMsg('Saved'),
      _ => this.showMsg('Communication error. Data not saved', true));
  }
}

