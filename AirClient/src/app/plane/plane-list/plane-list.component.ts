import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { PlaneListDataSource } from './plane-list-datasource';
import { PlaneApiService } from '../../rest-connector';

@Component({
  selector: 'app-plane-list',
  templateUrl: './plane-list.component.html',
  styleUrls: ['./plane-list.component.scss']
})
export class PlaneListComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: PlaneListDataSource;

  constructor(private planeApi: PlaneApiService) {}
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'name', 'createdDate', 'lifetime', 'commands'];

  ngOnInit() {
    this.dataSource = new PlaneListDataSource(this.planeApi, this.paginator, this.sort);
  }
}
