import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule,
  MatIconModule, MatListModule, MatTableModule,
  MatPaginatorModule, MatSortModule, MatGridListModule,
  MatCardModule, MatMenuModule, MatInputModule,
  MatSelectModule, MatRadioModule, MatSnackBar, MatSnackBarModule, MatAutocompleteModule, MatAutocomplete } from '@angular/material';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { FormBuilder } from 'ngx-strongly-typed-forms';

import { PilotApiService, PlaneTypeApiService, FlightsApiService, PlaneApiService,
  StewardessApiService, CrewApiService, DeparturesApiService } from './rest-connector';
import { PilotListComponent } from './pilot/pilot-list/pilot-list.component';
import { PlaneTypeListComponent } from './planeType/plane-type-list/plane-type-list.component';
import { PlaneListComponent } from './plane/plane-list/plane-list.component';
import { StewardessListComponent } from './stewardess/stewardess-list/stewardess-list.component';
import { FlightListComponent } from './flight/flight-list/flight-list.component';
import { DepartureListComponent } from './departure/departure-list/departure-list.component';
import { CrewListComponent } from './crew/crew-list/crew-list.component';
import { PlaneTypeDetailsComponent } from './planeType/plane-type-details/plane-type-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PlaneDetailsComponent } from './plane/plane-details/plane-details.component';
import { PilotDetailsComponent } from './pilot/pilot-details/pilot-details.component';
import { CrewDetailsComponent } from './crew/crew-details/crew-details.component';
import { StewardessDetailsComponent } from './stewardess/stewardess-details/stewardess-details.component';
import { FlightDetailsComponent } from './flight/flight-details/flight-details.component';
import { DepartureDetailsComponent } from './departure/departure-details/departure-details.component';

@NgModule({
  declarations: [
    AppComponent,
    PilotListComponent,
    PlaneTypeListComponent,
    PlaneListComponent,
    StewardessListComponent,
    FlightListComponent,
    DepartureListComponent,
    CrewListComponent,
    PlaneTypeDetailsComponent,
    PlaneDetailsComponent,
    PilotDetailsComponent,
    CrewDetailsComponent,
    StewardessDetailsComponent,
    FlightDetailsComponent,
    DepartureDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatAutocompleteModule
  ],
  providers: [
    HttpClient,
    FormBuilder,
    MatSnackBar,
    MatAutocomplete,
    PlaneTypeApiService,
    PlaneApiService,
    PilotApiService,
    StewardessApiService,
    CrewApiService,
    FlightsApiService,
    DeparturesApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
