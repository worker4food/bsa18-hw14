import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { FlightListDataSource } from './flight-list-datasource';
import { FlightsApiService } from '../../rest-connector';

@Component({
  selector: 'app-flight-list',
  templateUrl: './flight-list.component.html',
  styleUrls: ['./flight-list.component.scss']
})
export class FlightListComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: FlightListDataSource;

  constructor(private flightApi: FlightsApiService) {}
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'source', 'destination', 'departureDate', 'arrivalDate', 'freeTickets', 'commands'];

  ngOnInit() {
    this.dataSource = new FlightListDataSource(this.flightApi, this.paginator, this.sort);
  }
}
