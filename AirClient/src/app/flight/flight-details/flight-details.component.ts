import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, FormArray, FormControl, FormGroup } from 'ngx-strongly-typed-forms';
import { FlightDto, DeparturesApiService, FlightsApiService, FlightSchedDto, TicketPrefDto, TicketDto } from '../../rest-connector';
import { AbstractControl } from 'ngx-strongly-typed-forms';


@Component({
  selector: 'app-flight-details',
  templateUrl: './flight-details.component.html',
  styleUrls: ['./flight-details.component.scss']
})
export class FlightDetailsComponent implements OnInit {
  form = this.fb.group<FlightDto>({
    id: null,
    source: [null, Validators.required],
    destination: [null, Validators.required],
    departureDate: [null, Validators.required],
    arrivalDate: [null, Validators.required],
    tickets: this.fb.array<TicketDto>([])
  });
  title: string;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private informer: MatSnackBar,
    private flightApi: FlightsApiService) { }

  ngOnInit() {
    const id = this.route.snapshot.params.id;
    this.title = `Flight #${id}`;

    if (id !== 'new') {
      this.flightApi.flightsIdGet(id)
        .subscribe(x => {
          this.form.patchValue(x);

          for (const tc of x.tickets.map(t => this.fb.group(t))) {
            tc.disable();
            this.formTickets.push(tc);
          }
        });
    }
  }

  get formTickets(): FormArray<TicketDto> {
    return this.form.get('tickets') as FormArray<TicketDto>;
  }

  showMsg(msg: string, isError = false) {
    this.informer.open(msg, 'Close', {
      verticalPosition: 'top',
      duration: 3000,
      panelClass: isError ? 'error-msg' : 'info-msg'
    });
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    const val = this.form.value;
    const req = this.flightApi.flightsIdPut(val.id, val);

    req.subscribe(
      _ => this.showMsg('Saved'),
      _ => this.showMsg('Communication error. Data not saved', true));
  }
}
