import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge, forkJoin } from 'rxjs';
import { PilotDto, StewardessDto, PilotApiService, CrewApiService, StewardessApiService, CrewDto } from '../../rest-connector';

interface CrewInfo {
  id: number;
  pilot: PilotDto;
  stewardesses: StewardessDto[];
}
/**
 * Data source for the CrewList view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class CrewListDataSource extends DataSource<CrewInfo> {
  data: CrewInfo[] = [];

  constructor(
      private crewApi: CrewApiService,
      private pilotApi: PilotApiService,
      private stewApi: StewardessApiService,
      private paginator: MatPaginator,
      private sort: MatSort) {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<CrewInfo[]> {
    const dataO = forkJoin(
      this.crewApi.crewsGet(),
      this.pilotApi.pilotsGet()
    );

    dataO.subscribe(([crews, pilots]) => {
        this.data = this.prepareData(crews, pilots);
        this.paginator.length = crews.length;
    });

    return merge(dataO, this.paginator.page, this.sort.sortChange).pipe(
      map(() => this.getPagedData(this.getSortedData(this.data))));

}

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: CrewInfo[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.slice(startIndex, startIndex + this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: CrewInfo[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    const isAsc = this.sort.direction === 'asc';
    const field = this.sort.active;
    return data.sort((a, b) =>
      (a[field] < b[field] ? -1 : 1) * (isAsc ? 1 : -1));
  }

  private prepareData(crews: CrewDto[], pilots: PilotDto[]): CrewInfo[] {
    const pilotMap = pilots.reduce((acc, x) => {
      acc[x.id] = x;
      return acc;
    }, {});

    return crews.map(crew => ({
        id: crew.id,
        pilot: pilotMap[crew.pilotId],
        stewardesses: crew.stewardesses
    }));
  }
}
