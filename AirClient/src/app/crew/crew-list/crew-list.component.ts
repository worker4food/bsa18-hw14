import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { CrewListDataSource } from './crew-list-datasource';
import { CrewApiService, PilotApiService, StewardessApiService } from '../../rest-connector';

@Component({
  selector: 'app-crew-list',
  templateUrl: './crew-list.component.html',
  styleUrls: ['./crew-list.component.scss']
})
export class CrewListComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: CrewListDataSource;

  constructor(
    private crewApi: CrewApiService,
    private pilotApi: PilotApiService,
    private stewApi: StewardessApiService) {}
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'pilot', 'stewCount', 'commands'];

  ngOnInit() {
    this.dataSource = new CrewListDataSource(this.crewApi, this.pilotApi, this.stewApi, this.paginator, this.sort);
  }
}
