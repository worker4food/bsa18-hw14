import { Component, OnInit } from '@angular/core';
import { CrewDto, CrewApiService, PilotApiService, StewardessApiService, StewardessDto, PilotDto } from '../../rest-connector';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, FormArray, FormControl, FormGroup } from 'ngx-strongly-typed-forms';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-crew-details',
  templateUrl: './crew-details.component.html',
  styleUrls: ['./crew-details.component.scss']
})
export class CrewDetailsComponent implements OnInit {
  form = this.fb.group<CrewDto>({
    id: null,
    pilotId: [null, Validators.required],
    stewardesses: this.fb.array<StewardessDto>([])
  });

  pilots: PilotDto[] = [];
  stewardesses: StewardessDto[] = [];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private informer: MatSnackBar,
    private crewApi: CrewApiService,
    private pilotApi: PilotApiService,
    private stewApi: StewardessApiService) { }

  ngOnInit() {
    const id = this.route.snapshot.params.id;

    if (id !== 'new') {
      this.crewApi.crewsIdGet(id)
        .subscribe(x => {
          this.form.patchValue(x);

          for (const s of x.stewardesses) {
            this.formStewardesses.push(this.toFormControl(s));
          }
        });
    }

    forkJoin(this.pilotApi.pilotsGet(), this.stewApi.stewardessesGet())
      .subscribe(([p, s]) => {
        this.pilots = p;
        this.stewardesses = s;
      });
  }

  get formStewardesses() {
    return this.form.get('stewardesses') as FormArray<StewardessDto>;
  }

  toFormControl(s: StewardessDto) {
    return this.fb.group<StewardessDto>(s);
  }

  isNew(): boolean {
    return this.form.value.id === null;
  }

  showMsg(msg: string, isError = false) {
    this.informer.open(msg, 'Close', {
      verticalPosition: 'top',
      duration: 3000,
      panelClass: isError ? 'error-msg' : 'info-msg'
    });
  }

  title(): string {
    if (this.isNew()) {
      return 'New crew';
    } else {
      return `Crew #${this.form.value.id}`;
    }
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    const api = this.crewApi;
    const val = this.form.value;
    const req = api.crewsPost(val);

    req.subscribe(
      _ => this.showMsg('Saved'),
      _ => this.showMsg('Communication error. Data not saved', true));
  }

  addStewardess() {
    this.formStewardesses.push(this.toFormControl({
      id: null,
      firstName: '',
      lastName: '',
      birthDate: null
    }));
  }
}
