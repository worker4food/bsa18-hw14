import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge, forkJoin } from 'rxjs';
import { DepartureDto, DeparturesApiService, FlightDto, CrewDto, PlaneDto, FlightsApiService, PlaneApiService } from '../../rest-connector';

interface DepartureInfo {
  id: number;
  flight: FlightDto;
  plane: PlaneDto;
  departureDate?: Date;
}
/**
 * Data source for the DepartureList view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class DepartureListDataSource extends DataSource<DepartureInfo> {
  data: DepartureInfo[] = [];

  constructor(
      public departureApi: DeparturesApiService,
      public flightApi: FlightsApiService,
      public planeApi: PlaneApiService,
      private paginator: MatPaginator, private sort: MatSort) {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<DepartureInfo[]> {
    const dataO = forkJoin(
      this.departureApi.departuresGet(),
      this.flightApi.flightsGet(),
      this.planeApi.planesGet()
    );

    dataO.subscribe(([deps, flights, planes]) => {
        this.data = this.prepareData(deps, flights, planes);
        this.paginator.length = deps.length;
    });

    return merge(dataO, this.paginator.page, this.sort.sortChange).pipe(
      map(() => this.getPagedData(this.getSortedData(this.data))));

}

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: DepartureInfo[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.slice(startIndex, startIndex + this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: DepartureInfo[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    const isAsc = this.sort.direction === 'asc';
    const field = this.sort.active;
    return data.sort((a, b) =>
      (a[field] < b[field] ? -1 : 1) * (isAsc ? 1 : -1));
  }

  toIdDict(items: {id?: number | string}[]): any {
    return items.reduce((acc, x) => { acc[x.id] = x; return acc; }, {});
  }

  private prepareData(departures: DepartureDto[], flights: FlightDto[], planes: PlaneDto[]): DepartureInfo[] {
    const flightMap = this.toIdDict(flights);
    const planeMap = this.toIdDict(planes);

    return departures.map(d => ({
        id: d.id,
        flight: flightMap[d.flightId],
        plane: planeMap[d.planeId],
        departureDate: d.departureDate
    }));
  }
}
