import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DepartureListDataSource } from './departure-list-datasource';
import { DeparturesApiService, FlightsApiService, PlaneApiService } from '../../rest-connector';

@Component({
  selector: 'app-departure-list',
  templateUrl: './departure-list.component.html',
  styleUrls: ['./departure-list.component.scss']
})
export class DepartureListComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: DepartureListDataSource;

  constructor(
    private departureApi: DeparturesApiService,
    private flihgtApi: FlightsApiService,
    private planeApi: PlaneApiService) {}

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'departureDate', 'flightId', 'source', 'destination', 'commands'];

  ngOnInit() {
    this.dataSource = new DepartureListDataSource(this.departureApi, this.flihgtApi, this.planeApi, this.paginator, this.sort);
  }
}
