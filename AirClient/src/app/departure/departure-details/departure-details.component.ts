import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, FormArray, FormControl, FormGroup } from 'ngx-strongly-typed-forms';
import { DepartureDto, CrewDto, FlightDto, PlaneDto, PlaneApiService,
  FlightsApiService, CrewApiService, DeparturesApiService, PilotApiService, StewardessApiService } from '../../rest-connector';
import { forkJoin } from 'rxjs';

interface CrewInfo {
  id: number;
  pilot: string;
  stewardesses: string;
}

@Component({
  selector: 'app-departure-details',
  templateUrl: './departure-details.component.html',
  styleUrls: ['./departure-details.component.scss']
})
export class DepartureDetailsComponent implements OnInit {
  form = this.fb.group<DepartureDto>({
    id: null,
    flightId: [null, Validators.required],
    crewId: [null, Validators.required],
    planeId: [null, Validators.required],
    departureDate: null
  });

  flights: FlightDto[] = [];
  crews: CrewInfo[] = [];
  planes: PlaneDto[] = [];

  isNew: boolean;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private informer: MatSnackBar,
    private departureApi: DeparturesApiService,
    private flightsApi: FlightsApiService,
    private crewApi: CrewApiService,
    private planeApi: PlaneApiService,
    private pilotApi: PilotApiService,
    private stewApi: StewardessApiService) { }

  ngOnInit() {
    const id = this.route.snapshot.params.id;

    this.isNew = id === 'new';

    if (!this.isNew) {
      this.departureApi.departuresIdGet(id)
        .subscribe(x => this.form.setValue(x));
    }

    forkJoin([
        this.flightsApi.flightsGet(),
        this.crewApi.crewsGet(),
        this.planeApi.planesGet(),
        this.pilotApi.pilotsGet(),
        this.stewApi.stewardessesGet()
      ])
      .subscribe(([flights, crews, planes, pilots, stewardesses]) => {
        const pilotMap = pilots.reduce((acc, x) => {
          acc[x.id] = x;
          return acc;
        }, {});

        this.flights = flights;
        this.planes = planes;
        this.crews = crews.map(x => ({
          id: x.id,
          pilot: `${pilotMap[x.pilotId].firstName} ${pilotMap[x.pilotId].lastName}`,
          stewardesses: x.stewardesses.map(s => `${s.firstName} ${s.lastName}`).join(', ')
        }));
      });
  }

  showMsg(msg: string, isError = false) {
    this.informer.open(msg, 'Close', {
      verticalPosition: 'top',
      duration: 3000,
      panelClass: isError ? 'error-msg' : 'info-msg'
    });
  }

  title(): string {
    if (this.isNew) {
      return 'New departure';
    } else {
      return `Departure #${this.form.value.id}`;
    }
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    const api = this.departureApi;
    const val = this.form.value;
    const req = this.isNew ? api.departuresPost(val) : api.departuresIdPut(val.id, val);

    req.subscribe(
      _ => { this.showMsg('Saved'); this.isNew = false; },
      _ => this.showMsg('Communication error. Data not saved', true));
  }
}
