import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { StewardessDto, StewardessApiService } from '../../rest-connector';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, FormArray, FormControl, FormGroup } from 'ngx-strongly-typed-forms';

@Component({
  selector: 'app-stewardess-details',
  templateUrl: './stewardess-details.component.html',
  styleUrls: ['./stewardess-details.component.scss']
})
export class StewardessDetailsComponent implements OnInit {
  form = this.fb.group<StewardessDto>({
    id: null,
    firstName: [null, Validators.required],
    lastName: [null, Validators.required],
    birthDate: [null, Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private informer: MatSnackBar,
    private pilotApi: StewardessApiService) { }

  ngOnInit() {
    const id = this.route.snapshot.params.id;

    if (id !== 'new') {
      this.pilotApi.stewardessesIdGet(id)
        .subscribe(planeType => this.form.setValue(planeType));
    }
  }

  isNew(): boolean {
    return this.form.value.id === null;
  }

  showMsg(msg: string, isError = false) {
    this.informer.open(msg, 'Close', {
      verticalPosition: 'top',
      duration: 3000,
      panelClass: isError ? 'error-msg' : 'info-msg'
    });
  }

  title(): string {
    if (this.isNew()) {
      return 'New stewardess';
    } else {
      return `Stewardess #${this.form.value.id}`;
    }
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    const api = this.pilotApi;
    const val = this.form.value;
    const req = this.isNew() ? api.stewardessesPost(val) : api.stewardessesIdPut(val.id, val);

    req.subscribe(
      _ => this.showMsg('Saved'),
      _ => this.showMsg('Communication error. Data not saved', true));
  }
}

