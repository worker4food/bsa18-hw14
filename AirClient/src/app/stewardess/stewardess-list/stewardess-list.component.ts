import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { StewardessListDataSource } from './stewardess-list-datasource';
import { StewardessApiService } from '../../rest-connector';

@Component({
  selector: 'app-stewardess-list',
  templateUrl: './stewardess-list.component.html',
  styleUrls: ['./stewardess-list.component.scss']
})
export class StewardessListComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: StewardessListDataSource;

  constructor(private stewApi: StewardessApiService) {}
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'firstName', 'lastName', 'birthDate', 'commands'];

  ngOnInit() {
    this.dataSource = new StewardessListDataSource(this.stewApi, this.paginator, this.sort);
  }
}
