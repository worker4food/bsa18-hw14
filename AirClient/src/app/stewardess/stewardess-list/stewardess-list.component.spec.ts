
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { StewardessListComponent } from './stewardess-list.component';

describe('StewardessListComponent', () => {
  let component: StewardessListComponent;
  let fixture: ComponentFixture<StewardessListComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ StewardessListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StewardessListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
