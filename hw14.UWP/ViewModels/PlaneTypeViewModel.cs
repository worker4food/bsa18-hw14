﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System.Collections.ObjectModel;
using AirportApi;
using System.Windows.Input;

namespace hw14.ViewModels
{
    public class PlaneTypeViewModel : BaseViewModel
    {
        INavigationService _navigationService;

        public PlaneTypeViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;

            Title = "Plane types";

            GoBack = new RelayCommand(_navigationService.GoBack);
            Save = new RelayCommand(SaveItem);

            Task.Run(async () =>
            {
                var items = await (new Client()).PlaneTypesGetAsync();
               
                PlaneTypes = new ObservableCollection<PlaneTypeDto>(items);

                RaisePropertyChangedOnUI(() => PlaneTypes);
            });
        }

        public void SaveItem()
        {
            if (!IsPlaneTypeSelected)
                return;

            Task.Run(async () =>
            {
                var res = await new Client().PlaneTypesIdPutAsync(SelectedPlaneType.Id ?? -1, SelectedPlaneType);
                RaisePropertyChangedOnUI(() => SelectedPlaneType);
            });          
        }

        public ObservableCollection<PlaneTypeDto> PlaneTypes { get; private set; }

        PlaneTypeDto _selectedPlane;

        public PlaneTypeDto SelectedPlaneType
        {
            get => _selectedPlane;
            set {
                _selectedPlane = value;
                if (_selectedPlane != null)
                {
                    MessengerInstance.Send(_selectedPlane);
                }

                RaisePropertyChanged(() => SelectedPlaneType);
                RaisePropertyChanged(() => IsPlaneTypeSelected);
            }
        }

        public bool IsPlaneTypeSelected => _selectedPlane != null;

        public ICommand GoBack { get; private set; }
        public ICommand Save { get; private set; }
    }
}
