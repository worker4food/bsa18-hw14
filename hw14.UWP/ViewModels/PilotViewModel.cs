﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System.Collections.ObjectModel;
using AirportApi;
using System.Windows.Input;

namespace hw14.ViewModels
{
    public class PilotViewModel : BaseViewModel
    {
        INavigationService _navigationService;

        public PilotViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;

            Title = "Pilots";

            GoBack = new RelayCommand(_navigationService.GoBack);
            Save = new RelayCommand(SaveItem);

            Task.Run(async () =>
            {
                var items = await (new Client()).PilotsGetAsync();
               
                Pilots = new ObservableCollection<PilotDto>(items);

                RaisePropertyChangedOnUI(() => Pilots);
            });
        }

        public void SaveItem()
        {
            if (!IsSelected)
                return;

            Task.Run(async () =>
            {
                var res = await new Client().PilotsIdPutAsync(SelectedPilot.Id ?? -1, SelectedPilot);
                RaisePropertyChangedOnUI(() => SelectedPilot);
            });          
        }

        public ObservableCollection<PilotDto> Pilots { get; private set; }

        PilotDto _selectedPilot;

        public PilotDto SelectedPilot
        {
            get => _selectedPilot;
            set {
                _selectedPilot = value;
                if (_selectedPilot != null)
                {
                    MessengerInstance.Send(_selectedPilot);
                }

                RaisePropertyChanged(() => SelectedPilot);
                RaisePropertyChanged(() => IsSelected);
            }
        }

        public bool IsSelected => _selectedPilot != null;

        public ICommand GoBack { get; private set; }
        public ICommand Save { get; private set; }
    }
}
