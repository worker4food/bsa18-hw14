﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System.Collections.ObjectModel;
using AirportApi;
using System.Windows.Input;

namespace hw14.ViewModels
{
    public class StewardessViewModel : BaseViewModel
    {
        INavigationService _navigationService;

        public StewardessViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;

            Title = "Stewardesses";

            GoBack = new RelayCommand(_navigationService.GoBack);
            Save = new RelayCommand(SaveItem);

            Task.Run(async () =>
            {
                var items = await (new Client()).StewardessesGetAsync();
               
                Stewardesses = new ObservableCollection<StewardessDto>(items);

                RaisePropertyChangedOnUI(() => Stewardesses);
            });
        }

        public void SaveItem()
        {
            if (!IsSelected)
                return;

            Task.Run(async () =>
            {
                var res = await new Client().StewardessesIdPutAsync(SelectedStewardess.Id ?? -1, SelectedStewardess);
                RaisePropertyChangedOnUI(() => SelectedStewardess);
            });          
        }

        public ObservableCollection<StewardessDto> Stewardesses { get; private set; }

        StewardessDto _selectedStewardess;

        public StewardessDto SelectedStewardess
        {
            get => _selectedStewardess;
            set {
                _selectedStewardess = value;
                if (_selectedStewardess != null)
                {
                    MessengerInstance.Send(_selectedStewardess);
                }

                RaisePropertyChanged(() => SelectedStewardess);
                RaisePropertyChanged(() => IsSelected);
            }
        }

        public bool IsSelected => _selectedStewardess != null;

        public ICommand GoBack { get; private set; }
        public ICommand Save { get; private set; }
    }
}
