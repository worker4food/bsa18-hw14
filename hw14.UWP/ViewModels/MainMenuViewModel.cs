﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Views;
using System.Collections.ObjectModel;
using System.Windows.Input;
using hw14.Models;

namespace hw14.ViewModels
{
    public class MainMenuViewModel : BaseViewModel
    {
        private INavigationService _navigationService;

        public MainMenuViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;

            Title = "Imaginary Airport";
            Menu = new ObservableCollection<MenuItem>(new MainMenu().Menu);
        }

        public ObservableCollection<MenuItem> Menu { get; private set; }

        private MenuItem _selectedMenuItem;
        public MenuItem SelectedMenuItem
        {
            get { return _selectedMenuItem; }
            set
            {
                _selectedMenuItem = value;
                if (_selectedMenuItem != null)
                {
                    MessengerInstance.Send(_selectedMenuItem);
                    _navigationService.NavigateTo(_selectedMenuItem.Link);
                    _selectedMenuItem = null; // TODO: look closer
                }

                //RaisePropertyChanged(() => SelectedMenuItem);
            }
        }
    }
}
