﻿using System;
using System.Linq.Expressions;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Threading;

namespace hw14.ViewModels
{
    public class BaseViewModel : ViewModelBase
    {
        private string _title;
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                RaisePropertyChanged(() => Title);
            }
        }

        public void RaisePropertyChangedOnUI<T>(Expression<Func<T>> accessor)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(() => RaisePropertyChanged(accessor));
        }
    }
}
