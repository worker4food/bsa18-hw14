﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System.Collections.ObjectModel;
using AirportApi;
using System.Windows.Input;

namespace hw14.ViewModels
{
    public class PlaneViewModel : BaseViewModel
    {
        INavigationService _navigationService;
        Dictionary<long?, PlaneTypeDto> _planeTypesMap;

        public PlaneViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;

            Title = "Planes";

            GoBack = new RelayCommand(_navigationService.GoBack);
            Save = new RelayCommand(SaveItem);

            Task.Run(async () =>
            {
                var client = new Client();
                var items = await client.PlanesGetAsync();
                var planeTypes = await client.PlaneTypesGetAsync();

                Planes = new ObservableCollection<PlaneDto>(items);

                _planeTypesMap = planeTypes.ToDictionary(x => x.Id);
                PlaneTypes = new ObservableCollection<PlaneTypeDto>(_planeTypesMap.Values);

                RaisePropertyChangedOnUI(() => Planes);
                RaisePropertyChangedOnUI(() => PlaneTypes);
            });
        }

        public void SaveItem()
        {
            if (!IsSelected)
                return;

            Task.Run(async () =>
            {
                var res = await new Client().PlanesIdPutAsync(SelectedPlane.Id ?? -1, SelectedPlane);
                RaisePropertyChangedOnUI(() => SelectedPlane);
            });
        }

        public ObservableCollection<PlaneDto> Planes { get; private set; }

        PlaneDto _selectedPlane;

        public PlaneDto SelectedPlane
        {
            get => _selectedPlane;
            set {
                _selectedPlane = value;
                if (_selectedPlane != null)
                {
                    MessengerInstance.Send(_selectedPlane);
                }

                RaisePropertyChanged(() => SelectedPlane);
                RaisePropertyChanged(() => IsSelected);
                RaisePropertyChanged(() => SelectedPlaneType);
            }
        }

        public ObservableCollection<PlaneTypeDto> PlaneTypes { get; private set; }

        public PlaneTypeDto SelectedPlaneType
        {
            get => IsSelected ? _planeTypesMap[_selectedPlane.PlaneTypeId] : null;
            set
            {
                _selectedPlane.PlaneTypeId = value.Id ?? 0;

                RaisePropertyChanged(() => SelectedPlaneType);
                RaisePropertyChanged(() => SelectedPlane);
            }
        }

        public bool IsSelected => _selectedPlane != null;

        public ICommand GoBack { get; private set; }
        public ICommand Save { get; private set; }
    }
}
