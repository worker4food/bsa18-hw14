﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using Microsoft.Practices.ServiceLocation;
using hw14.ViewModels;
using hw14.Views;


namespace hw14
{
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            //CommonServiceLocator
            ServiceLocator.SetLocatorProvider (() => SimpleIoc.Default);

            var navigationService = new NavigationService();

            navigationService.Configure(nameof(MainMenuViewModel), typeof(MainMenuView));
            SimpleIoc.Default.Register<MainMenuViewModel>();
            
            navigationService.Configure(nameof(PlaneTypeViewModel), typeof(PlaneTypeView));
            SimpleIoc.Default.Register<PlaneTypeViewModel>();

            navigationService.Configure(nameof(PilotViewModel), typeof(PilotView));
            SimpleIoc.Default.Register<PilotViewModel>();

            navigationService.Configure(nameof(StewardessViewModel), typeof(StewardessView));
            SimpleIoc.Default.Register<StewardessViewModel>();

            navigationService.Configure(nameof(PlaneViewModel), typeof(PlaneView));
            SimpleIoc.Default.Register<PlaneViewModel>();

            if (ViewModelBase.IsInDesignModeStatic)
            {
                // Create design time view services and models
            }
            else
            {
                // Create run time view services and models
            }

            //Register your services used here
            //SimpleIoc.Default.Register<INavigationService, NavigationService>();
            SimpleIoc.Default.Register<INavigationService>(() => navigationService);

            ServiceLocator.Current.GetInstance<MainMenuViewModel>();
        }

        public MainMenuViewModel MainMenuVMInstance => ServiceLocator.Current.GetInstance<MainMenuViewModel>();
        public PlaneTypeViewModel PlaneTypeVMInstance => ServiceLocator.Current.GetInstance<PlaneTypeViewModel>();
        public PilotViewModel PilotVMInstance => ServiceLocator.Current.GetInstance<PilotViewModel>();
        public StewardessViewModel StewardessVMInstance => ServiceLocator.Current.GetInstance<StewardessViewModel>();
        public PlaneViewModel PlaneVMInstance => ServiceLocator.Current.GetInstance<PlaneViewModel>();

        // <summary>
        // The cleanup.
        // </summary>
        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}
