﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hw14.ViewModels;

namespace hw14.Models
{
    public class MenuItem
    {
        public string Title { get; set; }
        public string Link { get; set; }
    }

    public class MainMenu
    {
        public IEnumerable<MenuItem> Menu => new List<MenuItem>
        {
            new MenuItem { Title = "Plane types", Link = nameof(PlaneTypeViewModel) },
            new MenuItem { Title = "Planes", Link = nameof(PlaneViewModel) },
            new MenuItem { Title = "Pilots", Link =  nameof(PilotViewModel) },
            new MenuItem { Title = "Stewardesses", Link = nameof(StewardessViewModel) }
        };
    }
}
