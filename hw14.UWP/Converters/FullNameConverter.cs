﻿using System;
using Windows.UI.Xaml.Data;
using AirportApi;

namespace hw14.Converters
{
    class FullNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value is PilotDto)
                return FullName(value as PilotDto);
            else if (value is StewardessDto)
                return FullName(value as StewardessDto);
            else
                return "Bad object";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language) =>
            null;

        public static string FullName(PilotDto x) => $"{x.FirstName} {x.LastName}";
        public static string FullName(StewardessDto x) => $"{x.FirstName} {x.LastName}";
    }
}
