# BSA18 homework \#14 (11)

## UWP

```
git clone https://github.com/worker4food/bsa18-hw14.git
cd bsa18-hw14
```
Run backend app
```
dotnet run --project hw14.Web
```
Run UWP app from `hw14.uwp.sln`
